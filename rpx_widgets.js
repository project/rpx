(function ($) {

Drupal.behaviors.rpx = {
  attach: function (context, settings) {
    function popupSocial(post) {
      RPXNOW.loadAndRun(['Social'], function () {
        var activity = new RPXNOW.Social.Activity(
          post.label,
          post.linktext,
          post.link
        );
        if ('comment' in post) {
          activity.setUserGeneratedContent(post.comment);
        }
        if ('summary' in post) {
          activity.setDescription(post.summary);
        }
        RPXNOW.Social.publishActivity(activity);
      });
    };
    if ('rpx' in settings && 'atonce' in settings.rpx) {
      popupSocial(settings.rpx['atonce']);
    }
    $('.rpx-link-social', context).once('rpx-link-social', function() {
      var post = settings.rpx[$(this).attr('id')];
      $(this).bind('click', function(e) {popupSocial(post); return false;});
    });
  }
};

})(jQuery);
